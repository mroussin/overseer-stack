```markdown
# Media Management and Server Configuration

This repository contains a Docker Compose setup for managing a media server with Sonarr, Radarr, Overseerr, Prowlarr, Deluge, Bazarr, Doplarr, Flaresolverr, and Caddy.

## Prerequisites

- Docker and Docker Compose installed on your system.
- Ensure the `arr` network exists or create it with the following command:
  ```bash
  docker network create arr
  ```

## Getting Started

1. **Clone the Repository**:
   ```bash
   git clone <repository-url>
   cd <repository-directory>
   ```

2. **Configure Environment Variables**:
   - Edit the `.env.caddy` and `.env.doplarr` files to set the required environment variables.

### Caddy HTTPS Configuration with Cloudflare

Caddy can be configured to use HTTPS with automatic certificate renewal through Cloudflare.

1. **Cloudflare Setup**:
   - Ensure your domain is managed through Cloudflare.
   - Obtain your Cloudflare API token with appropriate permissions for DNS management.

2. **Caddy Configuration**:
   - Modify the `Caddyfile` to include the following for automatic certificate renewal:
     ```
     {env.ROOT_DOMAIN} *.{env.ROOT_DOMAIN} {
         reverse_proxy http://localhost:your_service_port
         tls {
             dns cloudflare {env.CLOUDFLARE_API_TOKEN}
         }
     }
     ```
   - Set your Cloudflare API token in the `.env.caddy` file:
     ```bash
     CLOUDFLARE_API_TOKEN=your_cloudflare_api_token
     ```

### Subdomains Configuration with Caddy

Caddy is configured to manage subdomains for each service using the `domains.caddy` file. Below is a breakdown of the subdomain configurations:

```caddyfile
  @sonarr host sonarr.YOUR_DOMAIN
  handle @sonarr {
    reverse_proxy sonarr:8989
  }

  @radarr host radarr.YOUR_DOMAIN
  handle @radarr {
    reverse_proxy radarr:7878
  }

  @overseerr host overseerr.YOUR_DOMAIN
  handle @overseerr {
    reverse_proxy overseerr:5055
  }

  @prowlarr host prowlarr.YOUR_DOMAIN
  handle @prowlarr {
    reverse_proxy prowlarr:9696
  }

  @bazarr host bazarr.YOUR_DOMAIN
  handle @bazarr {
    reverse_proxy bazarr:6767
  }

  @deluge host deluge.YOUR_DOMAIN
  handle @deluge {
    reverse_proxy deluge:8112
  }
```

3. **Restart Caddy**:
   - Apply the changes by restarting the Caddy service:
     ```bash
     docker-compose up -d caddy
     ```

Caddy will now handle HTTPS with automatic certificate renewal using Cloudflare DNS.

### Doplarr Configuration

Doplarr integrates with Sonarr and Radarr to provide assistant control.

1. **Environment Variables**:
   - Configure the `.env.doplarr` file with the necessary variables:
     - Example:
       ```bash
       DOPLARR_DISCORD_TOKEN=your_discord_bot_token
       DOPLARR_COMMAND_PREFIX=!
       ```

2. **Integration**:
   - Ensure that Doplarr is properly linked with your Discord bot and that the bot has the necessary permissions in your Discord server.

3. **Restart Doplarr**:
   - Apply any changes by restarting the Doplarr container:
     ```bash
     docker-compose up -d doplarr
     ```

Doplarr will now allow you to interact with your media server through Discord.

##

3. **Directory Structure**:
   - Ensure the following directories are available or create them:
     ```bash
     mkdir -p {sonarr,radarr,overseerr,prowlarr,deluge,bazarr,doplarr}/config caddy
     ```
   - Set up the appropriate paths for your media:
     - TV Shows: `/tank/plex-media/tv-shows/english`
     - Movies: `/tank/plex-media/movies/english`
     - Downloads: `$DOWNLOAD_DIR`

4. **Running the Services**:
   - Start all services with:
     ```bash
     docker-compose up -d
     ```
   - This will pull the necessary images and start the containers.

5. **Access the Services**:
   - **Sonarr**: [http://localhost:8989](http://localhost:8989)
   - **Radarr**: [http://localhost:7878](http://localhost:7878)
   - **Overseerr**: [http://localhost:5055](http://localhost:5055)
   - **Prowlarr**: [http://localhost:9696](http://localhost:9696)
   - **Deluge**: [http://localhost:8112](http://localhost:8112)
   - **Bazarr**: [http://localhost:6767](http://localhost:6767)
   - **Caddy**: [http://localhost:80](http://localhost:80)
   - **Flaresolverr**: [http://localhost:8191](http://localhost:8191)

6. **Stopping the Services**:
   ```bash
   docker-compose down
   ```

## Notes

- Caddy is configured to handle HTTPS and reverse proxy for the services.
- Adjust `PUID` and `PGID` if necessary, to match your user and group IDs.
- All containers are set to restart automatically unless stopped.

## Troubleshooting

- Check logs with:
  ```bash
  docker-compose logs -f
  ```
- Ensure proper permissions for directories and files.
- Modify the `docker-compose.yml` file as needed to fit your environment.

## License

This project is licensed under the MIT License.

```
This `README.md` should provide clear instructions on how to get started with your media server stack. Adjust as needed based on your environment and configuration preferences.
```
